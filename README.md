# Minimal Statuspage

Yet another statuspage. Demo: https://status.demo.j-serv.de/

## Quickstart

How to run with Docker:

```
docker run -it -p 8080:8080 hjacobs/minimal-statuspage:latest --sites https://example.org
```

Now open your browser at http://localhost:8080/

## Deploying into your Kubernetes cluster

This will deploy a single Pod with the status page into your cluster:

```
kubectl apply -k deploy/
kubectl port-forward service/minimal-statuspage 8080:80
```

Open http://localhost:8080/ in your browser to see the UI.

## Building the Docker image

```
make
```

## Developing Locally

```
make run
```
